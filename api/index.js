'use strict';

var express = require('express');
var app = express();
var fs = require('fs');
var https = require('https');

//var http = require('http').Server(app);
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var configDB = require('./config/database.js');

var hskey = fs.readFileSync( __dirname + '/certs/mysite-key');
var hscert = fs.readFileSync(__dirname + '/certs/mysite-cert.pem');


// connect to database
mongoose.connect(configDB.url);

// passport configuration
require('./config/passport')(passport);

app.use(morgan('dev')); // log requests to console
app.use(cookieParser()); // read cookies for auth
app.use(bodyParser.json()); // get info from html forms
app.use(bodyParser.urlencoded({ extended: false }));

// use ejs templating
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// require for passport
app.use(session({
	secret: 'ilovescotchscotchyscotchscotch',
	resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use('/', express.static(__dirname + '/../public'));

// routes
require('./routes.js')(app, passport);


https.createServer({
	key: hskey,
	cert: hscert
}, app).listen(8000);
